import axios from "axios";

const url = "https://store.fastandclever.com/api/v1";

export const getProducts = (lang) => axios.get(url + `/products/${lang}/`);

export const setCheckout = (id, checkout, lang = "en") =>
  axios.patch(url + `/product/${lang}/update-checkout/${id}`, { checkout });
