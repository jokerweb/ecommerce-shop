import { useState, useEffect } from "react";
import * as Api from "../../api/api";
import languagesJSON from "../../../assets/language.json";

export default () => {
  //state
  const [language, setLanguage] = useState(
    localStorage.getItem("_lang") || "en"
  );
  const [items, setItems] = useState([]);
  const [texts, setTexts] = useState(languagesJSON.en);
  const [isSubmit, setSubmit] = useState(false);

  const url = "https://store.fastandclever.com";

  //actions
  const getProducts = async () => {
    const { data } = await Api.getProducts(language);
    setItems(data);
  };

  useEffect(() => {
    setTexts(languagesJSON[language]);
    getProducts();
    return localStorage.setItem("_lang", language);
  }, [language]);

  useEffect(() => {
    getProducts();
  }, []);

  return {
    items,
    language,
    setLanguage,
    texts,
    url,
    isSubmit,
    setSubmit,
  };
};
