import { useState, useEffect } from "react";

export default () => {
  const [userId, setUserId] = useState("");
  const [token, setToken] = useState("");

  const setCookie = (val, id) => {
    if (id) {
      document.cookie = id;
      setUserId(id.split("=")[1]);
    }
    if (val) {
      document.cookie = val;
      setToken(val.split("=")[1]);
    }
    const token = document.cookie;
    // forceUpdate();
    if (token && token.includes("token=")) {
      return token;
    }
  };

  const deleteCookies = () => {
    // const splitted = document.cookie.split("; ");
    // const cookieId = splitted
    //   .find((el) => el.startsWith("userId="))
    //   .split("=")[1];
    let copyString = document.cookie.split("; ");

    copyString.forEach((item, i) => {
      if (item.startsWith("userId=") || item.startsWith("token=")) {
        document.cookie = `${item}; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;

        return;
      }
      document.cookie = item;
    });
    setToken("");
    setUserId("");
  };

  useEffect(() => {
    const splitted = document.cookie.split("; ");
    if (!splitted.find((el) => el.startsWith("userId="))) return;
    const cookieId = splitted
      .find((el) => el.startsWith("userId="))
      .split("=")[1];
    const cookieToken = splitted
      .find((el) => el.startsWith("token="))
      .split("=")[1];
    setToken(cookieToken);
    setUserId(cookieId);
  }, []);

  let isLogged = token && userId ? true : false;

  return { setCookie, token, userId, deleteCookies, isLogged };
};
