import React from "react";

const HR = ({ color,width }) => {
  return (
    <hr
      style={{
        backgroundColor: color,
        width,
        color,
        border: 0,
        height: 0.5,
      }}
    />
  );
};

export default HR;
