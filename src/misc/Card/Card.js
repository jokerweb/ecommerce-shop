import React, { useRef, useState, useEffect, useContext } from "react";
import s from "./Card.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart, faCheck } from "@fortawesome/free-solid-svg-icons";
import Button from "../Button/Button";
import { Link, useHistory } from "react-router-dom";
import MainContext from "../../store/context/MainState/MainContext";
import { Transition } from "react-transition-group";

const transitionStyles = {
  entering: { opacity: 1, transform: "rotate(0deg)" },
  entered: { opacity: 1, transform: "rotate(0deg)" },
  exiting: { opacity: 0.4, transform: "rotate(45deg)" },
  exited: { opacity: 0.4, transform: "rotate(45deg)" },
};

const Card = ({ item, isSmall, style, imageStyle }) => {
  const [localCheckout, setLocalCheckout] = useState(
    localStorage.getItem("checkout") || ""
  );
  const { url } = useContext(MainContext);

  const { description, title, keywords, price, mainImg, productId } = item;
  const [isAnimation, setAnimation] = useState(true);

  const h = useHistory();

  const linkRef = useRef();

  const checkoutPress = () => {
    const ids = localStorage.getItem("checkout");
    let finalStr = ids || "";
    setAnimation(false);
    setTimeout(() => {
      if (ids && ids.includes(productId)) {
        finalStr = finalStr.replace(productId + ",", "");
      } else {
        finalStr += productId + ",";
      }
      localStorage.setItem("checkout", finalStr);
      setLocalCheckout(localStorage.getItem("checkout"));
      h.push(h.location);
      setAnimation(true);
    }, 200);
  };

  useEffect(() => {
    setLocalCheckout(localStorage.getItem("checkout"));
  }, []);

  return (
    <div
      className={isSmall ? `${s.card} ${s.card__sm}` : `${s.card}`}
      {...{ style }}
    >
      <main className={s.card__main}>
        <Link
          ref={linkRef}
          to={{
            pathname: `/product/${productId}/${title
              .toLowerCase()
              .split(" ")
              .join("-")}`,
            state: { item },
          }}
        >
          <picture>
            <source
              type="image/webp"
              srcSet={
                url +
                "/public/assets/products/" +
                productId +
                "/" +
                mainImg +
                ".webp"
              }
            />
            <source
              type="image/png"
              srcSet={
                url +
                "/public/assets/products/" +
                productId +
                "/" +
                mainImg +
                ".png"
              }
            />
            <img
              src={
                url +
                "/public/assets/products/" +
                productId +
                "/" +
                mainImg +
                ".webp"
              }
              alt=""
              className={s.imageStyle}
            />
          </picture>
          {/* <iframe
            className={s.imageStyle}
            width="100%"
            height="100%"
            frameBorder="none"
            allow="autoplay"
            title={title}
            src={url + "/assets/products/" + productId + "/" + mainImg}
          ></iframe> */}
          {/* <video alt="loading" style={imageStyle} controls autoPlay>
            <source type='video/mp4'
              src={url + "/assets/products/" + productId + "/" + mainImg}
            ></source>
          </video> */}
        </Link>

        <div className={s.card__description}>
          <Link
            to={{
              pathname: `/product/${productId}/${title
                .toLowerCase()
                .split(" ")
                .join("-")}`,
              state: { item },
            }}
            className={s.card__description__body}
          >
            <div className={s.card__tags}>
              {keywords.split(",").map((keywoard, i) => (
                <div className={s.card__tag} key={i}>
                  <p>{keywoard}</p>
                </div>
              ))}
            </div>

            <h3>{title}</h3>
            {/* <p className={s.descCard}>{description}</p> */}
          </Link>
          <Transition in={isAnimation} timeout={200}>
            {(state) => (
              <div
                className={
                  !!localStorage.hasOwnProperty("checkout") &&
                  localCheckout.split(",").filter((id) => +id === productId)
                    .length
                    ? `${s.icon__container} ${s.active__icon__container}`
                    : `${s.icon__container}`
                }
                onClick={checkoutPress}
              >
                <FontAwesomeIcon
                  icon={
                    !!localStorage.hasOwnProperty("checkout") &&
                    localCheckout.split(",").filter((id) => +id === productId)
                      .length
                      ? faCheck
                      : faShoppingCart
                  }
                  style={transitionStyles[state]}
                  className={s.card__description__icon}
                  color="#fff"
                />
              </div>
            )}
          </Transition>
        </div>
      </main>

      <footer className={s.card__footer}>
        <p>${price}</p>
        <Button
          color={
            !!localStorage.hasOwnProperty("checkout") &&
            localCheckout.split(",").filter((id) => +id === productId).length
              ? "#2ec738"
              : "#f7a619"
          }
          text={
            !!localStorage.hasOwnProperty("checkout") &&
            localCheckout.split(",").filter((id) => +id === productId).length
              ? "In cart"
              : "buy"
          }
          size="md"
          onClick={checkoutPress}
          isUppercase={true}
        />
      </footer>
    </div>
  );
};

export default Card;
