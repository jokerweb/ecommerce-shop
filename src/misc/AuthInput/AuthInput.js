import React, { useState, useEffect, useContext } from "react";
import s from "./AuthInput.module.css";
import MainContext from "../../store/context/MainState/MainContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamation } from "@fortawesome/free-solid-svg-icons";
import InputMask from "react-input-mask";

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const AuthInput = ({
  validationType,
  onChange,
  isRequired = true,
  type = "text",
  onFocus,
  className: customClasses = "",
  containerClass,
  validationObject,
  setValidationObject,
  mask,
  ...rest
}) => {
  const [errorMessage, setErrorMessage] = useState("");
  const { texts } = useContext(MainContext);

  const focusHandler = (e) => {
    const copy = { ...validationObject };
    copy.validation[validationType].isTouched = true;
    setValidationObject(copy);
    if (onFocus) onFocus(e);
  };

  const validate = (e) => {
    const { value } = e.target;
    const copy = { ...validationObject };
    switch (validationType) {
      case "email":
        copy.validation[validationType].isValid = emailRegex.test(value);
        break;
      case "password":
        copy.validation[validationType].isValid = value.length >= 8;
        break;
      case "phone":
        // copy.validation[validationType].isValid ;
        break;
      default:
        copy.validation[validationType].isValid = !!value.length;
        break;
    }
    setValidationObject(copy);
    onChange(e);
  };

  const { isValid, isTouched } = validationObject.validation[validationType];

  useEffect(() => {
    if (!isValid && isTouched) {
      setErrorMessage(texts.error[validationType]);
    } else {
      setErrorMessage("");
    }
  }, [isValid, isTouched]);
  let className =
    !isValid && isTouched ? `${s.input} ${s.error__input}` : s.input;
  className += customClasses;
  return (
    <div className={`${s.container} ${containerClass}`}>
      {mask ? (
        <InputMask {...{ mask }} {...rest} onChange={validate}>
          {(inputProps) => (
            <input
              {...inputProps}
              {...{ className }}
              type="tel"
            />
          )}
        </InputMask>
      ) : (
        <input
          onChange={validate}
          onFocus={focusHandler}
          {...{ className }}
          {...{ type }}
          {...rest}
        />
      )}
      {errorMessage && (
        <span className={s.error__message}>
          <FontAwesomeIcon icon={faExclamation} className={s.error__icon} />
          {errorMessage}
        </span>
      )}
    </div>
  );
};

export default AuthInput;
