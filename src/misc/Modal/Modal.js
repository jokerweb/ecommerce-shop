import React from "react";
import s from "./Modal.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";

const Modal = ({ type = "default", message = "", isVisible = false }) => {
  let modalClass = `${s.modal} `;
  modalClass += `${s[type]} `;
  return (
    <div className={isVisible ? `${modalClass} ${s.active}` : modalClass}>
      <FontAwesomeIcon icon={faTimesCircle} className={s.icon} />
      <span>{message}</span>
    </div>
  );
};

export default Modal;
