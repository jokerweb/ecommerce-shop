import React, { useContext } from "react";
import s from "./Footer.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPhone,
  faMailBulk,
  faMapMarked,
} from "@fortawesome/free-solid-svg-icons";
import HR from "../HR/HR";
import MainContext from "../../store/context/MainState/MainContext";
import { Link, useLocation } from "react-router-dom";

const Footer = (props) => {
  const location = useLocation();
  const hideFooter = window.innerWidth < 600 &&
    (location.pathname === "/login" || location.pathname === "/register");

  const { texts } = useContext(MainContext);
  const { rights, contact } = texts.footer;

  return (
    !hideFooter && (
      <>
        <HR color="#dfdfdf" />
        <div>
          <div className={s.footer}>
            <div className={s.footer__main}>
              <img alt="loading" src={require("../../assets/logo.png")} />
              <div className={s.footer__info}>
                <Link to="/public-offer">Public Offer</Link>
                <br />
                <Link to="/politics">Politics</Link>
              </div>
            </div>

            <div className={s.footer__contact}>
              <h3>{contact}</h3>
              <div className={s.contact__block}>
                <FontAwesomeIcon icon={faPhone} />
                <p>+38(067) 990 50 90</p>
              </div>
              <div className={s.contact__block}>
                <FontAwesomeIcon icon={faMailBulk} />
                <p>support@fastandclever.com</p>
              </div>
              <div className={s.contact__block}>
                <FontAwesomeIcon icon={faMapMarked} />
                <p>Ukraine, Ternopil, Hrushevskoho St. 23</p>
              </div>
            </div>
          </div>
        </div>
        <HR color="#dfdfdf" />
        <div className={s.footer__rights}>
          <p>{rights}</p>
        </div>
      </>
    )
  );
};

export default Footer;
