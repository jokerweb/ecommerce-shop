import React, { useState, useEffect } from "react";
import s from "./Counter.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";

const Counter = ({ onChange }) => {
  const [value, setValue] = useState(1);

  const onPlusClick = () => setValue((prev) => prev + 1);
  const onMinusClick = () =>
    setValue((prev) => (prev - 1 > 0 ? prev - 1 : prev));

  useEffect(() => {
    onChange(value);
  }, [value]);

  return (
    <div>
      <FontAwesomeIcon
        onClick={onMinusClick}
        className={s.icon}
        icon={faMinus}
      />
      <input {...{ value }} className={s.input} />
      <FontAwesomeIcon onClick={onPlusClick} className={s.icon} icon={faPlus} />
    </div>
  );
};

export default Counter;
