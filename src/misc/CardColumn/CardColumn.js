import React, { useState, useContext, useEffect } from "react";
import s from "./CardColumn.module.css";
import MainContext from "../../store/context/MainState/MainContext";
import { useHistory, Link } from "react-router-dom";

const CardColumn = ({ item }) => {
  const [localCheckout, setLocalCheckout] = useState(
    localStorage.getItem("checkout") || ""
  );

  const { url } = useContext(MainContext);
  const { description, title, keywords, price, mainImg, productId } = item;

  const h = useHistory();

  const checkoutPress = () => {
    const ids = localStorage.getItem("checkout");
    let finalStr = ids || "";
    if (ids && ids.includes(productId)) {
      finalStr = finalStr.replace(productId + ",", "");
    } else {
      finalStr += productId + ",";
    }
    localStorage.setItem("checkout", finalStr);
    setLocalCheckout(localStorage.getItem("checkout"));
    h.push(h.location);
  };

  useEffect(() => {
    setLocalCheckout(localStorage.getItem("checkout"));
  }, []);
  return (
    <div className={s.checkout__card__container}>
      <div key={item.productId} className={s.checkout__card}>
        <Link to={`/single/${item.productId}`}>
          <div className={s.checkout__card__main}>
            <picture>
              <source
                type="image/webp"
                srcSet={
                  url +
                  "/public/assets/products/" +
                  productId +
                  "/" +
                  mainImg +
                  ".webp"
                }
              />
              <source
                type="image/png"
                srcSet={
                  url +
                  "/public/assets/products/" +
                  productId +
                  "/" +
                  mainImg +
                  ".png"
                }
              />
              <img
                src={
                  url +
                  "/public/assets/products/" +
                  productId +
                  "/" +
                  mainImg +
                  ".webp"
                }
                alt=""
                className={s.imageStyle}
              />
            </picture>
            {/* <iframe
              className={s.imageStyle}
              width="50%"
              height="250px"
              frameBorder="none"
              title={title}
              allow="autoplay"
              src={url + "/assets/products/" + productId + "/" + mainImg}
            ></iframe> */}
            <div className={s.checkout__card__desc}>
              <h3>{title}</h3>
              <p>{description}</p>
              <h3 className={s.checkout__card__price}>{"$" + price}</h3>
            </div>
          </div>
        </Link>
        <div className={s.card__tags}>
          {keywords.split(",").map((keywoard, i) => (
            <div className={s.card__tag} key={i}>
              <p>{keywoard}</p>
            </div>
          ))}
        </div>
      </div>

      <div className={s.checkout__options}>
        <button
          className={
            !!localStorage.hasOwnProperty("checkout") &&
            localCheckout.split(",").filter((id) => +id === productId).length
              ? `${s.button} ${s.button_md} ${s.active_button}`
              : `${s.button} ${s.button_md}`
          }
          onClick={() => checkoutPress(productId)}
        >
          {!!localStorage.hasOwnProperty("checkout") &&
          localCheckout.split(",").filter((id) => +id === productId).length
            ? `In cart`
            : `Add`}
        </button>
      </div>
    </div>
  );
};

export default CardColumn;
