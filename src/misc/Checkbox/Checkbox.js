import React from "react";
import "./Checkbox.css";

const Checkbox = ({ label }) => {
  return (
    <div className="checkbox__container">
      <input type="checkbox" checked />
      <p>{label}</p>
    </div>
  );
};

export default Checkbox;
