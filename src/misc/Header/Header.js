import React, { useState, useContext, useEffect } from "react";
import s from "./Header.module.css";
import { Link, useLocation } from "react-router-dom";
import MainContext from "../../store/context/MainState/MainContext";
import { slide as Menu } from "react-burger-menu";
import { Transition } from "react-transition-group";
import logo from "../../assets/white-icon.png";
import ProfileContext from "../../store/context/AuthState/ProfileContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faShoppingCart,
  faKey,
  faUser,
} from "@fortawesome/free-solid-svg-icons";

// const selectImages = {
//   en: require("../../assets/usa.svg"),
//   ua: require("../../assets/ukraine.svg"),
//   rus: require("../../assets/rus.svg"),
// };

const Header = () => {
  const { userId } = useContext(ProfileContext);

  const location = useLocation();

  const { texts } = useContext(MainContext);
  const { buttons } = texts.header;
  // const [isSelectOpened, setSelectOpened] = useState(false);
  const [barOpen, setBarOpen] = useState(false);
  // const [userId] = useState(
  //   setCookie() ? setCookie().split(";")[0].split("=")[1] : null
  // );

  // const switchSelectOpen = () => setSelectOpened(!isSelectOpened);
  useEffect(() => {
  }, [userId]);
  const [isCounterAnimation, setCounterAnimation] = useState(true);
  const [counter, setCounter] = useState(
    localStorage.getItem("checkout") || ""
  );

  // const selectHandler = (data) => {
  //   setLanguage(data);
  //   setSelectOpened(false);
  // };

  // const selectStyles = {
  //   control: (base) => ({ ...base, backgroundColor: "#404040" }),
  //   menu: (base) => ({ ...base, backgroundColor: "#404040", color: "#fff" }),
  // };

  // const options = ["en", "ua", "rus"];

  useEffect(() => {
    setCounter(localStorage.getItem("checkout"));
    setCounterAnimation(false);
    setTimeout(() => {
      setCounterAnimation(true);
    }, 100);
  }, [localStorage.getItem("checkout")]);
  const isAuth =
    location.pathname === "/login" || location.pathname === "/register";
  return (
    <header className={`${s.header} ${s.header__background}`}>
      <div className={s.header__inner}>
        <a href="https://fastandclever.com" className={s.img_link}>
          <img alt="loading" src={logo} />
        </a>{" "}
        <div className={s.content}>
          <div className={s.header__left}>
            <div className={s.links}>
              <Link
                className={
                  location.pathname === "/"
                    ? `${s.link} ${s.active}`
                    : `${s.link}`
                }
                to="/"
              >
                <FontAwesomeIcon icon={faHome} className={s.icon} />
                {buttons[0]}
              </Link>
              {/* <Link
                className={
                  location.pathname === "/products"
                    ? `${s.link} ${s.active}`
                    : `${s.link}`
                }
                to="/products"
              >
                <FontAwesomeIcon icon={faReceipt} className={s.icon} />
                {buttons[1]}
              </Link> */}
              <Link
                className={
                  location.pathname === "/bag"
                    ? `${s.link} ${s.active}`
                    : `${s.link}`
                }
                style={{ paddingRight: "35px" }}
                to="/bag"
              >
                <FontAwesomeIcon icon={faShoppingCart} className={s.icon} />
                {buttons[2]}
                {counter && (
                  <Transition in={isCounterAnimation} timeout={100}>
                    {(state) => {
                      const styles = {
                        entering: { opacity: 1, transform: "scale(1)" },
                        entered: { opacity: 1, transform: "scale(1)" },
                        exiting: { opacity: 1, transform: "scale(1.4)" },
                        exited: { opacity: 1, transform: "scale(1.4)" },
                      };
                      return (
                        <span style={styles[state]} className={s.cart__counter}>
                          {counter.split(",").length - 1}
                        </span>
                      );
                    }}
                  </Transition>
                )}
              </Link>
            </div>
          </div>
          {/* <Select
            styles={selectStyles}
            {...{ options }}
            closeMenuOnSelect
            menuIsOpen={isSelectOpened}
            onFocus={() => setSelectOpened(true)}
            onBlur={() => setSelectOpened(false)}
            components={{
              Option: ({ data }) => (
                <div
                  className={s.flag_container}
                  onClick={() => selectHandler(data)}
                >
                  <img
                    alt="load"
                    className={`${s.flag_image} ${s.flag_image_option}`}
                    src={selectImages[data]}
                  />
                  <p className={s.flag_text}>{data}</p>
                </div>
              ),
              SingleValue: ({ data }) => (
                <img
                  alt="load"
                  onClick={switchSelectOpen}
                  className={s.flag_image}
                  src={selectImages[data.value]}
                />
              ),
            }}
            value={{ value: language }}
            isSearchable={false}
          /> */}
          <div className={s.header__right}>
            <Link
              className={
                location.pathname === "/login" ||
                location.pathname === `/profile/${userId}` ||
                location.pathname === "/register"
                  ? `${s.link} ${s.active}`
                  : `${s.link}`
              }
              to={!userId ? "/login" : `/profile/${userId}`}
            >
              {!userId ? (
                <FontAwesomeIcon icon={faKey} className={s.icon} />
              ) : (
                <FontAwesomeIcon icon={faUser} className={s.icon} />
              )}
              {!userId ? buttons[3] : buttons[4]}
            </Link>
          </div>
        </div>
        <button
          className={s.burger}
          type="button"
          onClick={() => {
            setBarOpen(!barOpen);
          }}
        >
          <div className={!barOpen ? s.burger__btn : s.btnX}></div>
        </button>
        <Menu
          width="50%"
          right
          isOpen={barOpen}
          burgerButtonClassName={s.menu_hidden}
          noOverlay
          menuClassName={s.menu_color}
          crossButtonClassName={s.exit_hidden}
          bmMenuWrap={s.menu_width}
          disableAutoFocus
        >
          <div
            onClick={() => {
              setBarOpen(!barOpen);
            }}
          >
            <Link to="/" className={s.header__link}>
              <p className={s.sideLink}>
                <FontAwesomeIcon icon={faHome} className={s.icon} />
                {buttons[0]}
              </p>
            </Link>
            {/* <Link className={s.header__link} to="/products">
              <p className={s.sideLink}>{buttons[1]}</p>
            </Link> */}
            <Link className={s.header__link} to="/bag">
              <p className={s.sideLink}>
                <FontAwesomeIcon icon={faShoppingCart} className={s.icon} />
                {buttons[2]}
                {counter && (
                  <span className={s.cart__counter}>
                    {counter.split(",").length - 1}
                  </span>
                )}
              </p>
            </Link>

            <div className={s.header__right}>
              <Link
                className={s.header__link}
                to={!userId ? "/login" : `/profile/${userId}`}
              >
                <p className={s.sideLink}>
                  {!userId ? (
                    <FontAwesomeIcon icon={faKey} className={s.icon} />
                  ) : (
                    <FontAwesomeIcon icon={faUser} className={s.icon} />
                  )}
                  {!userId ? buttons[3] : buttons[4]}

                  {/* {!userId ? buttons[3] : buttons[4]} */}
                </p>
              </Link>
            </div>
            {/* <Select
              {...{ options }}
              styles={selectStyles}
              closeMenuOnSelect
              menuIsOpen={isSelectOpened}
              onFocus={() => setSelectOpened(true)}
              onBlur={() => setSelectOpened(false)}
              components={{
                Option: ({ data }) => (
                  <div
                    className={s.flag_container}
                    onClick={() => selectHandler(data)}
                  >
                    <img
                      alt="load"
                      className={s.flag_image}
                      src={selectImages[data]}
                    />
                    <p className={s.flag_text}>{data}</p>
                  </div>
                ),
                SingleValue: ({ data }) => (
                  <img
                    alt="load"
                    onClick={switchSelectOpen}
                    className={s.flag_image}
                    src={selectImages[data.value]}
                  />
                ),
              }}
              value={{ value: language }}
              isSearchable={false}
            /> */}
          </div>
        </Menu>
      </div>
    </header>
  );
};

export default Header;
