import React from "react";
import s from "./Button.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Button = ({
  onClick,
  text,
  size = "md",
  color = "#f7a619",
  isUppercase = false,
  icon,
}) => {
  let styles = `${s.button} ${s[`button_${size}`]}`;
  if (isUppercase) styles += ` ${s.uppercase}`;
  if (icon) styles += ` ${s.button__icon}`;
  
  return !icon ? (
    <button className={styles} style={{ background: color }} {...{ onClick }}>
      {text}
    </button>
  ) : (
    <button className={styles} style={{ background: color }} {...{ onClick }}>
      <FontAwesomeIcon {...{ size }} {...{ icon }} />
    </button>
  );
};

export default Button;
