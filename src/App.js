import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import "./App.css";
import Home from "./pages/Home/Home";
import Bag from "./pages/Bag/Bag";
import MainState from "./store/context/MainState/MainState";
import Profile from "./pages/Profile/Profile";
import ProfileState from "./store/context/AuthState/ProfileState";
import Single from "./pages/Single/Single";
import Footer from "./misc/Footer/Footer";
import Header from "./misc/Header/Header";
import Login from "./pages/Login/Login";
import NewRegister from "./pages/Register/Register";
import PublicOffer from "./misc/PublicOffer/PublicOffer";
import Politics from "./misc/Politics/Politics";
import MainContext from "./store/context/MainState/MainContext";
import { useEffect } from "react";
import Success from "./pages/Success/Success";
import Logout from "./pages/Logout/Logout";
// import ReactGA from "react-ga";
// import { createBrowserHistory } from "history";
import ProfileContext from "./store/context/AuthState/ProfileContext";
import CustomDevSingle from "./pages/CustomDevSingle/CustomDevSingle";

const PrivateRoute = ({
  redirectTo,
  component: Component,
  condition,
  state,
  ...rest
}) => (
  <Route {...rest}>
    {condition ? (
      <Component />
    ) : (
      <Redirect to={{ pathname: redirectTo, state }} />
    )}
  </Route>
);

// ReactGA.initialize("UA-169213210-2");

const App = () => {
  // const history = createBrowserHistory();

  // history.listen(({location}) => {
  //   ReactGA.set({ page: location.pathname }); // Update the user's current page
  //   ReactGA.pageview(location.pathname); // Record a pageview for the given page
  // });
  // ReactGA.initialize("UA-169213210-2");

  const mainStore = MainState();
  const profileStore = ProfileState();
  const [token, setToken] = useState(null);

  const getUserByToken = () => {
    return token ? true : false;
  };

  useEffect(() => {
    setToken(profileStore.token);
  }, [profileStore.token]);

  // useEffect(() => {
  //   ReactGA.initialize("UA-169213210-2");
  // }, []);

  return (
    <ProfileContext.Provider value={profileStore}>
      <MainContext.Provider value={mainStore}>
        <Router>
          <Header userId={profileStore.userId} />
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/bag">
              <Bag />
            </Route>
            {/* <Route path="/products">
              <Products />
            </Route> */}
            <Route path="/product/:id/:name">
              <Single />
            </Route>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/register">
              <NewRegister />
            </Route>
            <Route path="/public-offer">
              <PublicOffer />
            </Route>
            <Route path="/politics">
              <Politics />
            </Route>
            <Route path="/product/custom-development">
              <CustomDevSingle />
            </Route>
            <PrivateRoute
              path="/profile/:id"
              component={Profile}
              condition={getUserByToken}
              redirectTo="/login"
            />
            <PrivateRoute
              path="/success"
              component={Success}
              condition={getUserByToken}
              redirectTo="/"
            />
            <PrivateRoute
              path="/logout"
              component={Logout}
              condition={getUserByToken}
              redirectTo="/"
            />
          </Switch>
          <Footer />
        </Router>
      </MainContext.Provider>
    </ProfileContext.Provider>
  );
};

export default App;
