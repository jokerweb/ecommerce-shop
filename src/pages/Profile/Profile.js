import React, { useContext, useState, useEffect } from "react";
import axios from "axios";
import s from "./Profile.module.css";
import Input from "../../misc/Input/Input";
import {
  faAddressCard,
  faUser,
  faPhoneAlt,
  faEnvelope,
  faPencilAlt,
  faSignOutAlt,
} from "@fortawesome/free-solid-svg-icons";
import MainContext from "../../store/context/MainState/MainContext";
import { useParams, useHistory } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ProfileContext from "../../store/context/AuthState/ProfileContext";

const Profile = (props) => {
  const { id } = useParams();
  const { url } = useContext(MainContext);
  const { token } = useContext(ProfileContext);
  const [profileInfo, setProfileInfo] = useState(null);
  const [profileData, setProfileData] = useState();
  const h = useHistory();

  const fetchUser = () => {
    token &&
      axios
        .get(url + `/api/v1/profile/${id}`, {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then(
          (res) =>
            // if (document.cookie.length) {
            setProfileInfo(res.data)
          // h.push(h.location);
          // } else {
          // h.push("/login");
          // }
        )
        .catch((err) => {
          if (err) {
            h.push("/login");
          }
        });
    // : h.push("/login");
  };

  const onChange = (target, val) => {
    setProfileData({ ...profileData, [val]: target.value });
  };

  const editProfile = () => {
    axios
      .patch(
        url + `/api/v1/user/update/${id}`,
        {
          ...profileData,
          phone: +profileData.phone,
          password: profileInfo.password,
          roleId: profileInfo.roleId,
        },
        {
          headers: {
            Authorization: "Bearer " + token,
          },
        }
      )
      .then((res) => console.log(res))
      .catch((err) =>
        setTimeout(() => {
          alert("Error while updating profile");
          h.push("/login");
        }, 200)
      );
  };

  const logout = () => {
    h.push("/logout");
  };

  useEffect(() => {
    if (!token) return;
    window.scrollTo(0, 0);
    fetchUser();
  }, [token]);

  return (
    profileInfo && (
      <div>
        <div className={`${s.profile} container cont__margin`}>
          <div className={s.profile__main}>
            <div className={s.profile__info}>
              <div className={s.profile__info__fields}>
                <div className={s.profile__info__title}>
                  <span>Personal details</span>

                  <FontAwesomeIcon
                    icon={faSignOutAlt}
                    onClick={logout}
                    className={s.profile__info__icon}
                  />
                </div>

                <div className={s.profile__info__field}>
                  <Input
                    label="Name"
                    val={"firstName"}
                    icon={faUser}
                    defaultValue={profileInfo.firstName}
                    onChange={onChange}
                  />
                </div>
                <div className={s.profile__info__field}>
                  <Input
                    label="Surname"
                    val={"lastName"}
                    icon={faAddressCard}
                    defaultValue={profileInfo.lastName}
                    onChange={onChange}
                  />
                </div>
                <div className={s.profile__info__field}>
                  <Input
                    label="Phone"
                    val={"phone"}
                    type={"tel"}
                    icon={faPhoneAlt}
                    defaultValue={profileInfo.phone}
                    onChange={onChange}
                  />
                </div>
                <div className={s.profile__info__field}>
                  <Input
                    val={"email"}
                    label="E-mail"
                    icon={faEnvelope}
                    defaultValue={profileInfo.email}
                    onChange={onChange}
                  />
                </div>

                <button className={s.save__profile__btn} onClick={editProfile}>
                  Edit
                  <span className={s.profile__btn__overlay}>
                    <FontAwesomeIcon
                      icon={faPencilAlt}
                      className={s.profile__btn__overlay__icon}
                    />
                  </span>
                </button>
              </div>
            </div>
            {/* <img
              alt="loading"
              // src={
              //   users.avatar
              //     ? users.avatar
              //     : require("../../assets/avatarDefault.png")
              // }
            /> */}
          </div>

          {/* <Button icon={faSignOutAlt} text="Logout" onClick={logout} /> */}
        </div>
      </div>
    )
  );
};

export default Profile;
