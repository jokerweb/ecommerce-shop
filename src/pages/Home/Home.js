import React, { useContext, useEffect, useState, useRef, useMemo } from "react";
import s from "./Home.module.css";
import Typing from "react-typing-animation";
import Card from "../../misc/Card/Card";
import MainContext from "../../store/context/MainState/MainContext";
import Button from "../../misc/Button/Button";
import {
  faSearch,
  faTh,
  faList,
  faCaretSquareDown,
  faTimesCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import useWindowDimensions from "../../utils/UseWindowDimensions";
import CardColumn from "../../misc/CardColumn/CardColumn";
import Input from "../../misc/Input/Input";

const Home = (props) => {
  const { items, texts } = useContext(MainContext);
  const [products, setProducts] = useState(items);
  const { home, products: productsText } = texts;
  const { header, bestProjects } = home;
  const { filter } = productsText;

  const [isFilterVisible, setFilterVisible] = useState(false);
  const switchFilterVisible = () => setFilterVisible((prev) => !prev);

  const [isLangChanged, setLangChanged] = useState(false);
  const typistRef = useRef();
  const searchScroll = useRef();
  const radios = useRef();

  const { width: screenWidth } = useWindowDimensions();
  const [productsView, setProductsView] = useState("row");
  const [iconStyles, setIconStyles] = useState({});
  const [iconName, setIconName] = useState(faTh);
  const [productsOpacity, setProductsOpacity] = useState(1);
  const [checkedRadio, setCheckedRadio] = useState(null);

  const switchProductsView = () => {
    setIconStyles({ transform: "scale(0.1)" });
    setProductsOpacity(0.5);
    setTimeout(() => {
      setProductsView((prevState) => (prevState === "row" ? "column" : "row"));
      setIconName((prevState) => (prevState === faList ? faTh : faList));
      setIconStyles({ transform: "scale(1)" });
      setProductsOpacity(1);
    }, 500);
  };

  const clearFilters = () => {
    setCheckedRadio(null);
  };

  const SearchValue = (target) => {
    target.value.length === 0
      ? setProducts(items)
      : setProducts(
          items.filter((card) =>
            card.title.toLowerCase().includes(target.value.toLowerCase())
              ? card
              : null
          )
        );
  };

  const searchScrollHandler = () => {
    window.scrollTo({
      top: searchScroll.current.offsetTop - 80,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    if (typistRef && typistRef.current && typistRef.current.state.toType) {
      setLangChanged(true);
    }
  }, [texts]);

  useEffect(() => {
    setProducts(items);
  }, [items]);

  const headerTitle = useMemo(() => {
    return !isLangChanged ? (
      <Typing startDelay={1000} ref={typistRef}>
        <h1>{header.title}</h1>
      </Typing>
    ) : (
      <h1>{header.title}</h1>
    );
  }, [header.title]);

  const isMobile = screenWidth <= 750;

  useEffect(() => {
    setProducts(
      checkedRadio
        ? items.filter((card) => card.categoryId === checkedRadio)
        : items
    );
  }, [checkedRadio]);

  return (
    !!products && (
      <div>
        <header className={s.main_header}>
          <div className={s.search}>
            {headerTitle}
            <h3 className={s.subtitle}>{header.subtitle}</h3>
            <div className={s.search__main}>
              <input
                placeholder={header.inputPlaceholder}
                onChange={({ target }) =>
                  setProducts(
                    items.filter((item) =>
                      item.title
                        .toLocaleLowerCase()
                        .includes(target.value.toLocaleLowerCase())
                    )
                  )
                }
              />
              <Button
                text={header.searchButton}
                icon={window.innerWidth <= 650 ? faSearch : null}
                onClick={searchScrollHandler}
                size="lg"
              />
            </div>
          </div>
        </header>
        <main className={`${s.cards_container} ${s.container}`}>
          <h2>{bestProjects.title}</h2>
          <h4>{bestProjects.subtitle}</h4>
          <div className={s.products__container} ref={searchScroll}>
            <div className={s.products} style={{ opacity: productsOpacity }}>
              {products.map((item) => {
                if (item.deletedAt || item.categoryId === 3) return;
                return productsView === "row" ? (
                  <Card
                    {...{ item }}
                    isSmall={true}
                    imageStyle={{ height: "20vw" }}
                    key={item.productId}
                    itemsInRow={2}
                  />
                ) : (
                  <CardColumn {...{ item }} key={item.productId} />
                );
              })}
            </div>
            <div className={s.products__filter}>
              <div className={s.filter__title}>
                <span>{filter.title}</span>
                <div className={s.change__view__container}>
                  <FontAwesomeIcon
                    className={s.change__view__icon}
                    style={iconStyles}
                    onClick={
                      isMobile ? switchFilterVisible : switchProductsView
                    }
                    icon={isMobile ? faCaretSquareDown : iconName}
                  />
                </div>
              </div>
              {(!isMobile || isFilterVisible) && (
                <div className={s.filter__items}>
                  <div className={s.filter__search}>
                    <h5>Search</h5>
                    <Input
                      icon={faSearch}
                      style={{
                        padding: "0.5rem 5px",
                        borderRadius: "25px",
                      }}
                      inputStyle={{ marginTop: "15px" }}
                      onChange={SearchValue}
                    />
                  </div>
                  <h5>Categories</h5>
                  <div className={s.filter__checkboxes}>
                    <div className={s.checkbox__container}>
                      {["mobile app", "website", "CRM", "CMS"].map(
                        (category, i) =>
                          !!items.filter((item) => item.categoryId === i + 1)
                            .length && (
                            <div className={s.filter__category} key={i}>
                              <label>
                                <input
                                  checked={checkedRadio === i + 1}
                                  name="radio-group"
                                  type="radio"
                                  value={i + 1}
                                  ref={radios}
                                  onChange={() => {
                                    setCheckedRadio(i + 1);
                                  }}
                                />
                                {category}
                              </label>
                              {/* <Button
                                text={"Clear selected"}
                                icon={
                                  window.innerWidth <= 650
                                    ? faTimesCircle
                                    : null
                                }
                                onClick={() => {
                                  console.log(radios.current);
                                  radios.current.selected = false;
                                  radios.current.checked = false;

                                  setCheckedRadio(!checkedRadio);
                                  setProducts(items);
                                }}
                                size="lg"
                              /> */}
                            </div>
                          )
                      )}
                    </div>
                  </div>
                </div>
              )}
              <button onClick={clearFilters} className={s.clear__filter}>
                Clear
              </button>
            </div>
          </div>
        </main>
      </div>
    )
  );
};

export default Home;
