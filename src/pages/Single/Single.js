import React, { useState, useContext, useEffect, useRef } from "react";
import styles from "./Single.module.css";
import { useParams, useHistory, Link, Redirect } from "react-router-dom";
import "@brainhubeu/react-carousel/lib/style.css";
import Carousel from "@brainhubeu/react-carousel";
import ScrollListener from "react-scroll-listener";

import MainContext from "../../store/context/MainState/MainContext";
import HR from "../../misc/HR/HR";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowUp } from "@fortawesome/free-solid-svg-icons";
import ProfileContext from "../../store/context/AuthState/ProfileContext";

const Single = () => {
  const { items, url } = useContext(MainContext);
  const { isLogged } = useContext(ProfileContext);
  const { id } = useParams();
  const scrollListener = new ScrollListener();
  const [localCheckout, setLocalCheckout] = useState(
    localStorage.getItem("checkout")
  );
  const [product, setProduct] = useState(
    items.filter((item) => item.productId === +id)[0]
  );
  const [activeImageId, setActiveImageId] = useState(1);
  const [isTopButtonVisible, setTopButtonVisible] = useState(false);

  const h = useHistory();
  const desc = useRef();
  const faq = useRef();
  const review = useRef();
  const log = useRef();

  const scrollToRef = (ref) => {
    window.scrollTo({
      top: ref.current.offsetTop - 80,
      behavior: "smooth",
    });
  };

  const orderHandler = () => {
    localStorage.setItem("checkout", product.productId + ",");
    h.push("/bag");
  };

  const checkoutPress = () => {
    const ids = localStorage.getItem("checkout");
    let finalStr = ids || "";
    if (ids && ids.includes(id)) {
      finalStr = finalStr.replace(id + ",", "");
    } else {
      finalStr += id + ",";
    }
    localStorage.setItem("checkout", finalStr);
    setLocalCheckout(localStorage.getItem("checkout"));
    h.push(h.location);
  };

  const onScroll = (e) => {
    let scrollTop = window.pageYOffset;
    // itemTranslate = Math.min(0, scrollTop / 3 - 60);
    if (scrollTop >= 600) {
      setTopButtonVisible(true);
    } else if (scrollTop < 600) {
      setTopButtonVisible(false);
    }
  };

  useEffect(() => {
    setProduct(items.filter((item) => item.productId === +id)[0]);
  }, [items]);

  useEffect(() => {
    scrollListener.addScrollHandler("1", onScroll);
    window.scrollTo(0, 0);
  }, []);

  const upHandler = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  const images = product && product.gallery ? product.gallery.split(", ") : [];

  const imageClass =
    product && +product.categoryId === 2
      ? "carousel__item__img__web"
      : "carousel__item__img";

  // if(product.categoryId === 3 && )

  if (!isLogged && product && product.categoryId === 3) {
    return (
      <Redirect
        to={{
          pathname: "/login",
          state: { callbackUrl: "/product/4/custom-development" },
        }}
      />
    );
  }

  return product ? (
    <section {...{ onScroll }} className={`container ${styles.container}`}>
      <div className={styles.container__inner}>
        <div className={styles.pagination}>
          <Link to="/" className={styles.pagination__active}>
            Home /{" "}
          </Link>
          <span className={styles.pagination__current}>
            {product.title.split(" ").join("-")}
          </span>
        </div>
        <div className={styles.title__text}>{product.title}</div>
        <div className={styles.intro}>
          <div className={styles.intro__preview}>
            <div className={styles.carousel}>
              {images.length ? (
                <Carousel
                  slidesPerPage={window.innerWidth >= 600 ? 3 : 1}
                  slidesPerScroll={1}
                  // stopAutoPlayOnHover
                  minDraggableOffset={1}
                  animationSpeed={350}
                  value={activeImageId}
                  onChange={setActiveImageId}
                  itemWidth={
                    window.innerWidth >= 600 && product.categoryId === 2
                      ? 500
                      : 300
                  }
                  dots={true}
                  clickToChange
                  centered
                >
                  {images.map((img, i) => (
                    <div
                      key={i}
                      className={
                        activeImageId === i
                          ? `${styles.carousel__item} ${styles.carousel__item__active}`
                          : styles.carousel__item
                      }
                    >
                      <picture>
                        <source
                          srcSet={
                            url +
                            "/public/assets/products/" +
                            product.productId +
                            "/" +
                            img +
                            ".webp"
                          }
                          type="image/webp"
                        />
                        <source
                          srcSet={
                            url +
                            "/public/assets/products/" +
                            product.productId +
                            "/" +
                            img +
                            ".png"
                          }
                          type="image/png"
                        />
                        <img
                          className={styles[imageClass]}
                          alt=""
                          src={
                            url +
                            "/public/assets/products/" +
                            product.productId +
                            "/" +
                            img +
                            ".webp"
                          }
                        />
                      </picture>
                    </div>
                  ))}
                </Carousel>
              ) : (
                <div className={styles.carousel__item__mainImg}>
                  <picture>
                    <source
                      srcSet={
                        url +
                        "/public/assets/products/" +
                        product.productId +
                        "/" +
                        product.mainImg +
                        ".webp"
                      }
                      type="image/webp"
                    />
                    <source
                      srcSet={
                        url +
                        "/public/assets/products/" +
                        product.productId +
                        "/" +
                        product.mainImg +
                        ".png"
                      }
                      type="image/png"
                    />
                    <img
                      className={styles.mainImg}
                      alt=""
                      src={
                        url +
                        "/public/assets/products/" +
                        product.productId +
                        "/" +
                        product.mainImg +
                        ".webp"
                      }
                    />
                  </picture>
                </div>
              )}
            </div>
          </div>
          <div className={styles.actions__container}>
            <div className={styles.price}>
              <div className={styles.price__from}>Price from:</div>
              <div className={styles.price__count}>${product.price}</div>
            </div>
            <div className={`${styles.description__buttons} ${styles.cart}`}>
              <button
                onClick={checkoutPress}
                className={
                  localCheckout &&
                  localCheckout.split(",").filter((item) => +item === +id)
                    .length
                    ? styles.incart
                    : styles.description__button
                }
                type="button"
              >
                {localCheckout &&
                localCheckout.split(",").filter((item) => +item === +id).length
                  ? "In Cart"
                  : "Add to cart"}
              </button>
              <button
                onClick={orderHandler}
                className={`${styles.description__button} ${styles.buy}`}
                type="button"
              >
                Order
              </button>
            </div>
            <div className={styles.description__title}>
              <div className={styles.keywords}>
                <div className={styles.technologies}>Technologies:</div>
                <div className={styles.keywoards__block}>
                  {product.keywords.split(",").map((keyword, i) => (
                    <div className={styles.title__category} key={i}>
                      {keyword}
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div
              className={styles.keywords}
              style={{
                marginTop: "1rem",
                backgroundColor: "green",
                color: "white",
              }}
            >
              Free MoneyBack 14 days
            </div>
          </div>
        </div>
      </div>
      <div className={styles.intro__description}>
        <div className={styles.description__inner}>
          <div className={styles.description__top}>
            <div className={styles.container_block}>
              <div>
                <div>
                  <ul className={styles.lists}>
                    <li
                      className={`${styles.list_active} ${styles.list}`}
                      onClick={() => scrollToRef(desc)}
                    >
                      Description
                    </li>
                    <li
                      className={styles.list}
                      onClick={() => scrollToRef(log)}
                    >
                      ChangeLog
                    </li>
                    <li
                      className={styles.list}
                      onClick={() => scrollToRef(review)}
                    >
                      Review
                    </li>
                    <li
                      className={styles.list}
                      onClick={() => scrollToRef(faq)}
                    >
                      F.A.Q
                    </li>
                  </ul>
                </div>

                <div className={styles.list_block} ref={desc}>
                  <h3>Description</h3>
                  <p className={styles.list_info}>
                    {product.description}
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Incidunt iure laboriosam praesentium quam. At consequuntur,
                    necessitatibus ipsum maxime ad eos placeat impedit aperiam
                    incidunt atque consequatur magni aliquam minima in.
                  </p>
                </div>
                <HR color="orange" width="95%" />

                <div className={styles.list_block} ref={log}>
                  <h3>Changelog</h3>
                  <div className={styles.list_info}>
                    <p>Changelog.title</p>
                    <p>Changelog.description</p>
                    <p>Changelog.date</p>
                  </div>
                </div>
                <HR color="orange" width="95%" />

                <div className={styles.list_block} ref={review}>
                  <h3>Review</h3>
                  <div className={`${styles.dflex} ${styles.list_info}`}>
                    <div style={{ padding: "10px" }} className="avatar">
                      <img
                        style={{ borderRadius: "35px" }}
                        src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70"
                        alt=""
                      />
                    </div>
                    <div classn="comment-content">
                      <div className="arrow-comment"></div>
                      <div className="comment-by" style={{ padding: "5px" }}>
                        John Doe
                        <p className="date" style={{ padding: "5px 0" }}>
                          May 2019
                        </p>
                        <div className="star-rating" data-rating="4">
                          <span className="star"></span>
                          <span className="star"></span>
                          <span className="star"></span>
                          <span className="star"></span>
                          <span className="star empty"></span>
                        </div>
                      </div>
                      <p style={{ padding: "5px" }}>
                        Commodo est luctus eget. Proin in nunc laoreet justo
                        volutpat blandit enim. Sem felis, ullamcorper vel
                        aliquam non, varius eget justo. Duis quis nunc tellus
                        sollicitudin mauris.
                      </p>
                      <p
                        style={{
                          margin: "15px 5px",
                          padding: "10px",
                          border: "1px solid #cccccc",
                          borderRadius: "10px",
                        }}
                      >
                        <a
                          href="/rating"
                          className="rate-review"
                          style={{
                            textDecoration: "nproduct",
                            color: "#616161",
                          }}
                        >
                          <i className="sl sl-icon-like"></i> Helpful Review
                          <span>2</span>
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
                <HR color="orange" width="95%" />

                <div className={styles.list_block} ref={faq}>
                  <h3>FAQ</h3>
                  <div className={styles.list_info}>
                    <h4>Question Title</h4>
                    <p>Question asnwer</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {isTopButtonVisible && (
        <div className={styles.up} onClick={upHandler}>
          <FontAwesomeIcon icon={faArrowUp} color={"white"} />
        </div>
      )}
      {/* =================================================== */}

      {/* --------------------------------------------GOOGLE PAGE ----------------------------------- */}
      <div hidden>
        <div itemType="http://schema.org/Product" itemScope>
          <meta itemProp="mpn" content="925872" />
          <meta itemProp="name" content={product.title} />
          <link
            itemProp="image"
            href={url + "/public/assets/products/" + id + "/" + product.mainImg}
          />
          <link
            itemProp="image"
            href={url + "/public/assets/products/" + id + "/" + product.mainImg}
          />
          {/* <link
              itemProp="image"
              href="https://example.com/photos/1x1/photo.jpg"
            /> */}
          <meta itemProp="description" content={product.description} />
          <div itemProp="offers" itemType="http://schema.org/Offer" itemScope>
            <link itemProp="url" href="https://example.com/anvil" />
            <meta
              itemProp="availability"
              content="https://schema.org/InStock"
            />
            <meta itemProp="priceCurrency" content="USD" />
            <meta
              itemProp="itemCondition"
              content="https://schema.org/UsedCondition"
            />
            <meta itemProp="price" content={product.price} />
            <meta itemProp="priceValidUntil" content="2020-11-20" />
            <div
              itemProp="seller"
              itemType="http://schema.org/Organization"
              itemScope
            >
              <meta itemProp="name" content={product.title} />
            </div>
          </div>
          <div
            itemProp="aggregateRating"
            itemType="http://schema.org/AggregateRating"
            itemScope
          >
            <meta itemProp="reviewCount" content="89" />
            <meta itemProp="ratingValue" content="4.4" />
          </div>
          <div itemProp="review" itemType="http://schema.org/Review" itemScope>
            <div
              itemProp="author"
              itemType="http://schema.org/Person"
              itemScope
            >
              <meta itemProp="name" content="John Doe" />
            </div>
            <div
              itemProp="reviewRating"
              itemType="http://schema.org/Rating"
              itemScope
            >
              <meta itemProp="ratingValue" content="4" />
              <meta itemProp="bestRating" content="5" />
            </div>
          </div>
          <meta itemProp="sku" content="0446310786" />
          <div itemProp="brand" itemType="http://schema.org/Brand" itemScope>
            <meta itemProp="name" content="ACME" />
          </div>
        </div>
      </div>
    </section>
  ) : (
    <div />
  );
};

export default Single;
