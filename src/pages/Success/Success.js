import React, { useContext, useEffect } from "react";
import s from "./Success.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import MainContext from "../../store/context/MainState/MainContext";

const Success = (props) => {
  const { texts } = useContext(MainContext);
  const { title, subtitle, button } = texts.success;

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <div className={`${s.container}`}>
      <div className={s.content}>
        <div className={s.icon__container}>
          <FontAwesomeIcon icon={faCheck} className={s.icon} />
        </div>
        <div className={s.main}>
          <h1>{title}</h1>
          <h3>{subtitle}</h3>
          <Link to="/">
            <div className={s.button}>{button}</div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Success;
