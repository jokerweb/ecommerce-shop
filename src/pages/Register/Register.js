import React, { useState, useContext, useRef, useEffect } from "react";
import axios from "axios";
import s from "./Register.module.css";
import { Link, useHistory } from "react-router-dom";
import planet from "../../assets/planet.jpg";
import MainContext from "../../store/context/MainState/MainContext";
import AuthInput from "../../misc/AuthInput/AuthInput";
import ProfileContext from "../../store/context/AuthState/ProfileContext";
import Modal from "../../misc/Modal/Modal";

const NewRegister = () => {
  const [formData, setFormData] = useState({
    validation: {
      firstName: { isTouched: false, isValid: false },
      lastName: { isTouched: false, isValid: false },
      email: { isTouched: false, isValid: false },
      phone: { isTouched: false, isValid: false },
      password: { isTouched: false, isValid: false },
    },
    values: {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      password: "",
      roleId: 10,
    },
  });
  const [isModalVisible, setModalVisible] = useState(false);
  const [modalMessage, setModalMessage] = useState("");

  const form = useRef();
  const { url } = useContext(MainContext);
  const { setCookie } = useContext(ProfileContext);
  const h = useHistory();

  const setPhoneNumber = ({ target }) => {
    const { value } = target;
    let formattedPhone = "";
    formattedPhone = value.replace(/[+_-]/g, "");
    setFormData({
      ...formData,
      values: {
        ...formData.values,
        phone: formattedPhone,
      },
    });
  };

  const createUser = (e) => {
    e.preventDefault();
    form.current.reset();
    axios
      .post(url + "/api/v1/user/create", {
        ...formData.values,
        phone: +formData.values.phone,
      })
      .then((res) => {
        setCookie(`token=` + res.data.token);
        res.status === 200
          ? h.push(`/profile/${res.data.user.userId}`)
          : alert("res.status");
      })
      .catch((err) => {
        setModalMessage("Користувач з цими даними вже зареєстрований.");
        setModalVisible(true);
        setTimeout(() => {
          setModalVisible(false);
        }, 5000);
      });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <React.Fragment>
      <Modal isVisible={isModalVisible} message={modalMessage} type="error" />
      <div className={`${s.login__background} ${s.mobile_none}`}>
        <div className={s.login__background_img}>
          <div className={`${s.mask} ${s.lightblack} ${s.blur}`}>
            <div className={s.login__description_inner}>
              <div className={s.login__title} lang="en">
                Register your account
              </div>
              <div className={s.login__description}>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum.
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className={s.phone}>
        <div className={s.phone__body}>
          <div className={s.phone__background}>
            <img
              src={planet}
              alt="bgimage"
              className={s.phone__background_img}
            />
            <div className={s.maskphone}>
              {/* <button className="burger phone" type="button">
                <div className="burger__btn phone"></div>
              </button> */}

              <div className={s.phone__content}>
                <div className={s.phone__content_inner}>
                  <div className={s.log__sign}>
                    <div className={s.phone__login_title} lang="en">
                      Sign In
                    </div>
                  </div>

                  <div className={s.phone__forms}>
                    <form ref={form}>
                      <AuthInput
                        type="text"
                        validationType="firstName"
                        placeholder="First Name"
                        validationObject={formData}
                        setValidationObject={setFormData}
                        onChange={({ target }) =>
                          setFormData({
                            ...formData,
                            values: {
                              ...formData.values,
                              firstName: target.value,
                            },
                          })
                        }
                      />
                      <AuthInput
                        type="text"
                        validationType="lastName"
                        placeholder="Last Name"
                        validationObject={formData}
                        setValidationObject={setFormData}
                        onChange={({ target }) =>
                          setFormData({
                            ...formData,
                            values: {
                              ...formData.values,
                              lastName: target.value,
                            },
                          })
                        }
                      />
                      <AuthInput
                        validationObject={formData}
                        setValidationObject={setFormData}
                        type="tel"
                        validationType="phone"
                        placeholder="+380-99-999-9999"
                        mask="+380-99-999-9999"
                        onChange={setPhoneNumber}
                      />
                      <AuthInput
                        type="email"
                        validationObject={formData}
                        setValidationObject={setFormData}
                        validationType="email"
                        placeholder="E-mail"
                        onChange={({ target }) =>
                          setFormData({
                            ...formData,
                            values: {
                              ...formData.values,
                              email: target.value,
                            },
                          })
                        }
                      />

                      <AuthInput
                        type="password"
                        validationType="password"
                        validationObject={formData}
                        setValidationObject={setFormData}
                        placeholder="Password"
                        onChange={({ target }) =>
                          setFormData({
                            ...formData,
                            values: {
                              ...formData.values,
                              password: target.value,
                            },
                          })
                        }
                      />
                    </form>
                  </div>

                  <div className={s.phone__buttons}>
                    <button
                      className={s.phone__login_btn}
                      lang="en"
                      onClick={(e) => createUser(e)}
                    >
                      Register
                    </button>
                    <Link to="/login" className={s.phone__sign_a} lang="en">
                      <button className={s.phone__loginfacebook_btn} lang="en">
                        Login
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className={`${s.login__slogan} ${s.mobile_none}`}>
        <div className={s.login__slogan_inner}>
          <div className={s.login__slogan_text} lang="en">
            Become better with us!
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default NewRegister;
