import React, { useEffect, useContext } from "react";
import { useHistory } from "react-router-dom";
import ProfileContext from "../../store/context/AuthState/ProfileContext";
import s from "./Logout.module.css";
import MainContext from "../../store/context/MainState/MainContext";

const Logout = (props) => {
  const h = useHistory();
  const { deleteCookies } = useContext(ProfileContext);
  const { texts } = useContext(MainContext);

  // const deleteCookies = () => {
  //   document.cookie = "";
  //   // const deletedToken = `${document.cookie
  //   //   .split("; ")
  //   //   .find((id) => id.startsWith("token"))};max-age=-1`;
  //   // setCookie(deletedToken);

  //   // const deletedID = `${document.cookie
  //   //   .split("; ")
  //   //   .find((id) => id.startsWith("userId"))};max-age=-1`;
  //   setCookie("");

  // };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div className={`container cont__margin ${s.container}`}>
      <div className={s.main}>
        <h2>{texts.logout.title}</h2>
        <div className={s.button__container}>
          <button
            className={s.button}
            onClick={() => {
              deleteCookies();
              h.push("/");
            }}
          >
            {texts.yes}
          </button>
          <button
            className={s.cancel__button}
            onClick={() => {
              h.push("/");
            }}
          >
            {texts.no}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Logout;
