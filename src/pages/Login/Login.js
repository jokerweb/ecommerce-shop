import React, { useState, useContext, useEffect } from "react";
import axios from "axios";
import s from "./Login.module.css";
import { Link, useHistory } from "react-router-dom";
import planet from "../../assets/planet.jpg";
import MainContext from "../../store/context/MainState/MainContext";
import ProfileContext from "../../store/context/AuthState/ProfileContext";
import AuthInput from "../../misc/AuthInput/AuthInput";
import Modal from "../../misc/Modal/Modal";

const Login = (props) => {
  const [formData, setFormData] = useState({
    validation: {
      email: {
        isValid: false,
        isTouched: false,
      },
      password: { isValid: false, isTouched: false },
    },
    values: { email: "", password: "" },
  });

  const [isModalVisible, setModalVisible] = useState(false);
  const [modalMessage, setModalMessage] = useState("");

  const h = useHistory();
  const { url } = useContext(MainContext);
  const { setCookie } = useContext(ProfileContext);

  const handleLogin = async () => {
    await axios
      .post(url + "/api/v1/user/", formData.values)
      .then((res) => {
        if (res.status === 200) {
          // console.log(document.cookie);
          if (res.data.isAdmin) {
            h.push("/admin");
            return;
          }
          setCookie(
            `token=${res.data.token}`,
            `userId=${res.data.user.userId}`
          );
          // setUserId(res.data.user.userId);
          if (h.location.state && h.location.state.callbackUrl) {
            h.push(h.location.state.callbackUrl);
          } else {
            h.push(`/profile/${res.data.user.userId}`);
          }
        }
      })
      .catch((err) => {
        console.error(err);

        setModalMessage("Невірно введені дані");
        setModalVisible(true);
        setTimeout(() => {
          setModalVisible(false);
        }, 5000);
      });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const isFormValid = !Object.values(formData.validation).filter(
    (el) => !el.isValid
  ).length;

  return (
    <React.Fragment>
      <Modal isVisible={isModalVisible} message={modalMessage} type="error" />
      <div className={`${s.login__background} ${s.mobile_none}`}>
        <div className={s.login__background_img}>
          <div className={`${s.mask} ${s.lightblack} ${s.blur}`}>
            <div className={s.login__description_inner}>
              <div className={s.login__title} lang="en">
                Log In to your account
              </div>
              <div className={s.login__description}>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum.
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className={s.phone}>
        <div className={s.phone__body}>
          <div className={s.phone__background}>
            <img
              src={planet}
              alt="bgimage"
              className={s.phone__background_img}
            />
            <div className={s.maskphone}>
              {/* <button className="burger phone" type="button">
                <div className="burger__btn phone"></div>
              </button> */}

              <div className={s.phone__content}>
                <div className={s.phone__content_inner}>
                  <div className={s.log__sign}>
                    <div className={s.phone__login_title} lang="en">
                      Log In
                    </div>
                  </div>

                  <div className={s.phone__forms}>
                    <AuthInput
                      type="email"
                      validationType="email"
                      isRequired={true}
                      validationObject={formData}
                      setValidationObject={setFormData}
                      placeholder="example@example.com"
                      onChange={({ target }) =>
                        setFormData({
                          ...formData,
                          values: { ...formData.values, email: target.value },
                        })
                      }
                    />
                    <AuthInput
                      type="password"
                      validationType="password"
                      isRequired={true}
                      validationObject={formData}
                      setValidationObject={setFormData}
                      placeholder="••••••••"
                      onChange={({ target }) =>
                        setFormData({
                          ...formData,
                          values: {
                            ...formData.values,
                            password: target.value,
                          },
                        })
                      }
                    />
                  </div>

                  <div className={s.phone__buttons}>
                    <button
                      disabled={!isFormValid}
                      className={s.phone__login_btn}
                      lang="en"
                      onClick={handleLogin}
                    >
                      Log In
                    </button>
                    <Link to="/register" className={s.phone__sign_a} lang="en">
                      <button className={s.phone__loginfacebook_btn} lang="en">
                        Sign up
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className={`${s.login__slogan} ${s.mobile_none}`}>
        <div className={s.login__slogan_inner}>
          <div className={s.login__slogan_text} lang="en">
            Become better with us!
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Login;
