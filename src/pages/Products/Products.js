import React, { useContext, useState, useEffect } from "react";
import s from "./Products.module.css";
import MainContext from "../../store/context/MainState/MainContext";
import Card from "../../misc/Card/Card";
import Input from "../../misc/Input/Input";
import { faSearch, faTh, faList } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CardColumn from "../../misc/CardColumn/CardColumn";
import useWindowDimensions from "../../utils/UseWindowDimensions";

const Products = (props) => {
  const { items, texts } = useContext(MainContext);
  const { width: screenWidth } = useWindowDimensions();
  const { title, filter } = texts.products;
  // const [searchValue, setSeachValue] = useState();
  const [cards, setCards] = useState(items);
  const [productsView, setProductsView] = useState("row");
  const [iconStyles, setIconStyles] = useState({});
  const [iconName, setIconName] = useState(faTh);
  const [productsOpacity, setProductsOpacity] = useState(1);

  const switchProductsView = () => {
    setIconStyles({ transform: "scale(0.1)" });
    setProductsOpacity(0.5);
    setTimeout(() => {
      setProductsView((prevState) => (prevState === "row" ? "column" : "row"));
      setIconName((prevState) => (prevState === faList ? faTh : faList));
      setIconStyles({ transform: "scale(1)" });
      setProductsOpacity(1);
    }, 500);
  };

  const SearchValue = (target) => {
    target.value.length === 0
      ? setCards(items)
      : setCards(
          items.filter((card) =>
            card.title.toLowerCase().includes(target.value.toLowerCase())
              ? card
              : null
          )
        );
  };

  useEffect(() => {
    setCards(items);
  }, [items]);

  return (
    cards && (
      <div>
        <div className={`container cont__margin ${s.container}`}>
          <div className={s.header}>
            <h1>{title}</h1>
            {screenWidth >= 750 && (
              <div className={s.change__view__container}>
                <FontAwesomeIcon
                  className={s.change__view__icon}
                  style={iconStyles}
                  onClick={switchProductsView}
                  icon={iconName}
                />
              </div>
            )}
          </div>

          <div className={s.products__container}>
            <div className={s.products} style={{ opacity: productsOpacity }}>
              {cards.map((item) =>
                !item.deletedAt && productsView === "row" ? (
                  <Card
                    {...{ item }}
                    isSmall={true}
                    imageStyle={{ height: "20vw" }}
                    key={item.productId}
                    itemsInRow={2}
                  />
                ) : (
                  <CardColumn {...{ item }} key={item.productId} />
                )
              )}
            </div>
            <div className={s.products__filter}>
              <h3>{filter.title}</h3>
              <div className={s.filter__items}>
                <div className={s.filter__search}>
                  <h5>Search</h5>
                  <Input
                    icon={faSearch}
                    style={{
                      padding: "0.5rem 5px",
                      borderRadius: "25px",
                    }}
                    inputStyle={{ marginTop: "15px" }}
                    onChange={SearchValue}
                  />
                </div>
                <h5>Categories</h5>
                <div className={s.filter__checkboxes}>
                  <div className={s.checkbox__container}>
                    {!!items.filter((item) => item.categoryId === "1")
                      .length && (
                      <div>
                        <label htmlFor={"mobApp"}>
                          <input
                            name="radio-group"
                            type="radio"
                            value={1}
                            onChange={({ target }) =>
                              setCards(
                                items.filter(
                                  (card) => card.categoryId === target.value
                                )
                              )
                            }
                          />
                          mobile app
                        </label>
                      </div>
                    )}
                  </div>
                  <div className={s.checkbox__container}>
                    {!!items.filter((item) => item.categoryId === "2")
                      .length && (
                      <div>
                        <label htmlFor={"web"}>
                          <input
                            name="radio-group"
                            type="radio"
                            value={2}
                            onChange={({ target }) =>
                              setCards(
                                items.filter(
                                  (card) => card.categoryId === target.value
                                )
                              )
                            }
                          />
                          website
                        </label>
                      </div>
                    )}
                  </div>
                  <div className={s.checkbox__container}>
                    {!!items.filter((item) => item.categoryId === "3")
                      .length && (
                      <div>
                        <label htmlFor={"crm"}>
                          <input
                            name="radio-group"
                            type="radio"
                            value={3}
                            onChange={({ target }) =>
                              setCards(
                                items.filter(
                                  (card) => card.categoryId === target.value
                                )
                              )
                            }
                          />
                          CRM
                        </label>
                      </div>
                    )}
                  </div>
                  <div className={s.checkbox__container}>
                    {!!items.filter((item) => item.categoryId === "4")
                      .length && (
                      <div>
                        <label htmlFor={"cms"}>
                          <input
                            name="radio-group"
                            type="radio"
                            value={4}
                            onChange={({ target }) =>
                              setCards(
                                items.filter(
                                  (card) => card.categoryId === target.value
                                )
                              )
                            }
                          />
                          CMS
                        </label>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  );
};

export default Products;
