import React, { useContext, useState, useEffect } from "react";
import axios from "axios";
import s from "./Bag.module.css";
import MainContext from "../../store/context/MainState/MainContext";
import Button from "../../misc/Button/Button";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useHistory, Link } from "react-router-dom";
import ProfileContext from "../../store/context/AuthState/ProfileContext";

const Bag = (props) => {
  const h = useHistory();
  const { userId, token } = useContext(ProfileContext);
  // const [userId] = useState(setCookie().split(";")[0].split("=")[1]);

  const { url, items } = useContext(MainContext);
  const [fullPrice, setFullPrice] = useState(0);
  const [localCheckout, setLocalCheckout] = useState(
    localStorage.getItem("checkout") || ""
  );
  const [checkoutItems, setCheckoutItems] = useState(items);

  const submitOrder = () => {
    +userId &&
      axios
        .post(
          url + "/api/v1/order/create",
          {
            products: localCheckout,
            userId: parseInt(userId),
            orderSum: fullPrice,
            status: "created",
            notes: "created notes",
          },
          {
            headers: { Authorization: "Bearer " + token },
          }
        )
        .then((res) => {
          res.status === 200
            ? localStorage.setItem("checkout", "")
            : console.log("err");
        })
        .catch((err) => console.error("Сталась помилка", err));
  };

  const checkoutPress = (productId) => {
    const ids = localStorage.getItem("checkout");
    let finalStr = ids || "";
    if (ids && ids.includes(productId)) {
      finalStr = finalStr.replace(productId + ",", "");
    } else {
      finalStr += productId + ",";
    }
    localStorage.setItem("checkout", finalStr);
    setLocalCheckout(localStorage.getItem("checkout"));
    h.push(h.location);
  };

  useEffect(() => {
    if (!items.length) return;
    const array = [];
    localCheckout &&
      localCheckout.split(",").forEach((id, i) => {
        const filtered = items.filter((item) => {
          return item.productId === +id;
        });
        if (filtered.length) {
          array.push(filtered[0]);
        }
      });
    setCheckoutItems(array);

    let price = 0;
    array.forEach((item) => {
      price += item.price;
    });

    setFullPrice(price);
  }, [localCheckout, items]);

  useEffect(() => {
    window.scrollTo(0, 0);
    setLocalCheckout(localStorage.getItem("checkout"));
  }, []);

  return (
    <div className={s.checkout}>
      <div className={s.checkout_cards}>
        {checkoutItems.map(
          ({ productId, mainImg, title, price }) =>
            !!localCheckout.split(",").filter((id) => +id === productId)
              .length && (
              <div key={productId} className={s.checkout__card}>
                <Link
                  to={`/product/${productId}/${title
                    .split(" ")
                    .join("-")
                    .toLowerCase()}`}
                >
                  <div className={s.checkout__card__main}>
                    <picture>
                      <source
                        type="image/webp"
                        srcSet={
                          url +
                          "/public/assets/products/" +
                          productId +
                          "/" +
                          mainImg +
                          ".webp"
                        }
                      />
                      <source
                        type="image/png"
                        srcSet={
                          url +
                          "/public/assets/products/" +
                          productId +
                          "/" +
                          mainImg +
                          ".png"
                        }
                      />
                      <img
                        src={
                          url +
                          "/public/assets/products/" +
                          productId +
                          "/" +
                          mainImg +
                          ".webp"
                        }
                        alt=""
                        className={s.imageStyle}
                      />
                    </picture>
                    <div className={s.checkout__card__desc}>
                      <h3>{title}</h3>
                      {/* <p>{item.description}</p> */}
                      <h3 className={s.checkout__card__price}>{"$" + price}</h3>
                    </div>
                  </div>
                </Link>
                <div className={s.checkout__options}>
                  <FontAwesomeIcon
                    icon={faTrash}
                    className={s.btnTrash}
                    onClick={() => checkoutPress(productId)}
                  />
                  <button
                    className={`${s.button} ${s.button_md}`}
                    style={{ background: "red" }}
                    onClick={() => checkoutPress(productId)}
                  >
                    remove
                  </button>
                </div>
              </div>
            )
        )}
      </div>
      {!fullPrice ? (
        <p className={s.checkout__empty}>Your Cart is Empty</p>
      ) : (
        <div className={s.checkout__footer}>
          <h3>from</h3>
          <h2>{"$" + fullPrice}</h2>
          {/* <a href="https://www.interkassa.com"> */}
          <Link
            to={
              !+userId
                ? {
                    pathname: "/login",
                    state: { callbackUrl: "/bag" },
                  }
                : "/success"
            }
          >
            <Button
              onClick={!+userId ? () => {} : submitOrder}
              text="order"
              color="green"
              isUppercase
              size="lg"
            />
          </Link>
          {/* </a> */}
        </div>
      )}
    </div>
  );
};

export default Bag;
